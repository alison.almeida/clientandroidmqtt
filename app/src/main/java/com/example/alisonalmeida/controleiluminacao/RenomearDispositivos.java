package com.example.alisonalmeida.controleiluminacao;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RenomearDispositivos extends AppCompatActivity {

    private static final String PREFS_NAME = "Nomes";
    String[] KEY = new String[8];
    String[] Results = new String[8];
    Button[] btns = new Button[8];
    EditText[] txtEquipamentos = new EditText[8];
    Vibrator v;
    int idBtn, idEditText, i;

    //======= Objetos para salvar e carregar configurações ============//
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    //=================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renomear);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Renomear Dispositivos");                                     //Altera o titulo da janela
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        v = (Vibrator)getSystemService(VIBRATOR_SERVICE);

        /*settings = getSharedPreferences(PREFS_NAME, 0);
        String result = settings.getString("KEY", "");*/

        //-------------- Cria as 8 KEYS de configurações -------- //
        for(i = 0; i < 8; i++){
            KEY[i] = "KEY " + i;
        }

        //----- Instancia os botoes e as EditText's do layout -------//
        idBtn = R.id.btnDsp1;
        idEditText = R.id.txtEquipamento1;
        for (i = 0; i < 8; i++){
            this.btns[i] = (Button) findViewById(idBtn + i);
            this.txtEquipamentos[i] = (EditText) findViewById(idEditText + i);
            this.txtEquipamentos[i].setGravity(0);
        }

        //------ Recupera os 8 dados salvos como parametros ------//
        for (i = 0; i < 8; i++){
            settings = getSharedPreferences(PREFS_NAME, 0);
            Results[i] = settings.getString(KEY[i], "");
            if (!Results[i].isEmpty()){
                this.txtEquipamentos[i].setText(Results[i]);
            }
        }

        //-------------- Cria o evento de clique ----------------//
        for(i = 0; i < 8; i++){
            this.btns[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = v.getId() - idBtn;
                    Editar(id);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        getSupportActionBar().getDisplayOptions();
        if (id == 16908332){
            Intent it = new Intent();
            it.putExtra("NOMES", Results);
            setResult(3, it);
            super.onBackPressed();
            finish();
            return true;
        }
        Log.d("Title", "Titulo: " + id);

        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }
    private void alertShort(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    private void alertLong(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
    private void Editar(int id){
        v.vibrate(100);
        Results[id] = txtEquipamentos[id].getText().toString();
        settings = getSharedPreferences(PREFS_NAME, 0);
        editor = settings.edit();
        editor.putString(KEY[id], txtEquipamentos[id].getText().toString());
        editor.apply();
        alertShort("Renomeado com sucesso");
    }
    @Override
    public void onBackPressed() {
        Intent it = new Intent();
        it.putExtra("NOMES", Results);
        setResult(3, it);
        super.onBackPressed();
    }
}
