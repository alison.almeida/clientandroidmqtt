package com.example.alisonalmeida.controleiluminacao;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.StyleRes;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class Display extends AppCompatActivity {
    private int[] num = new int[12];
    private int[] numP = new int[10];
    private int[] dHumidade = new int[6];
    private int[] dTemperatura = new int[6];

    public Display(){
        num[0] = R.style.A0;
        num[1] = R.style.A1;
        num[2] = R.style.A2;
        num[3] = R.style.A3;
        num[4] = R.style.A4;
        num[5] = R.style.A5;
        num[6] = R.style.A6;
        num[7] = R.style.A7;
        num[8] = R.style.A8;
        num[9] = R.style.A9;
        num[10] = R.style.lC;
        num[11] = R.style.lGraus;

        numP[0] = R.style.P0;
        numP[1] = R.style.P1;
        numP[2] = R.style.P2;
        numP[3] = R.style.P3;
        numP[4] = R.style.P4;
        numP[5] = R.style.P5;
        numP[6] = R.style.P6;
        numP[7] = R.style.P7;
        numP[8] = R.style.P8;
        numP[9] = R.style.P9;

        dHumidade[0] = R.id.imgDig7;
        dHumidade[1] = R.id.imgDig8;
        dHumidade[2] = R.id.imgDig9;
        dHumidade[3] = R.id.imgDig10;
        dHumidade[4] = R.id.imgDig11;
        dHumidade[5] = R.id.imgDig12;

        dTemperatura[0] = R.id.imgDig1;
        dTemperatura[1] = R.id.imgDig2;
        dTemperatura[2] = R.id.imgDig3;
        dTemperatura[3] = R.id.imgDig4;
        dTemperatura[4] = R.id.imgDig5;
        dTemperatura[5] = R.id.imgDig6;
    }
    public int getNumber(int n){
        return num[n];
    }
    public int getNumberP(int n){
        return numP[n];
    }
    public int getLetter(){
        return num[10];
    }
    public int getSimbol(){
        return num[11];
    }
    public int getdHumidade(int d){
        return dHumidade[d];
    }
    public int getdTemperatura(int d){return dTemperatura[d]; }
}
