package com.example.alisonalmeida.controleiluminacao;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class NivelAgua extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Vibrator v;
    private Handler handler;
    Servidor servidor;
    private ImageView imageView;

    //=======================================//
    //Botoes
    Button modoBomba, dBomba, lBomba;
    boolean modo;
    //======================================//
    //Barra de Nivel
    ProgressBar barraNivel;

    //======================================//
    //Cores
    int verde, vermelho, soma = 0;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivel_agua);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        verde = getResources().getColor(R.color.botaoLigado);
        vermelho = getResources().getColor(R.color.botaoDesligado);

        this.modoBomba = (Button) findViewById(R.id.btnManAuto);
        modoBomba.setBackgroundColor(verde);

        this.lBomba = (Button) findViewById(R.id.btnLbomba);
        lBomba.setBackgroundColor(verde);

        this.dBomba = (Button) findViewById(R.id.btnDbomba);
        dBomba.setBackgroundColor(vermelho);

        this.barraNivel = (ProgressBar) findViewById(R.id.barraNivel);

        this.imageView = (ImageView) findViewById(R.id.imgCaixaDagua);
        final android.view.ContextThemeWrapper wrapper = new android.view.ContextThemeWrapper(this, R.style.cheio);
        changeTheme(wrapper.getTheme());

        setTitle("Controle de Nivel");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        v = (Vibrator)getSystemService(VIBRATOR_SERVICE);

        //------------ cria o receptor de mensagens -------------
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                UpDateUI(msg);
            }
        };

        servidor = new Servidor(handler);
        //=======================================================

        modo = false;
        if (modo){
            lBomba.setEnabled(false);
            dBomba.setEnabled(false);
        }

        //Modo das bomba
        modoBomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MAG", "Clicou");
                soma = soma + 10;
                barraNivel.setProgress(soma);
                if (modo){
                    modo = false;
                    lBomba.setEnabled(false);
                    dBomba.setEnabled(false);

                }
                else{
                    modo = true;
                    lBomba.setEnabled(true);
                    dBomba.setEnabled(true);
                }
            }
        });

        //Liga bomba
        lBomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //Desliga bomba;
        dBomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.renomearDispositivos) {
            Intent intent = new Intent(getContext(), RenomearDispositivos.class);
            Bundle params = new Bundle();
            startActivity(intent);
            return true;
        }

        else if (id == R.id.configRede){
            return true;
        }

        else if (id == R.id.sobre){
            //alert("A&A Sistemas Eletronicos \n                   1.0");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_iluminacao) {
            Intent intent = new Intent(this, main.class);
            Bundle params = new Bundle();
            startActivity(intent);
            return true;
        }

        else if (id == R.id.nav_temp_humi) {
            Intent intent = new Intent(this, Temperatura.class);
            Bundle params = new Bundle();
            startActivity(intent);
            return true;
        }

        else if (id == R.id.nav_nivel) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Context getContext(){
        return this;
    }

    private void alert (String s){
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case 1: {
                /*txtIpConectado.setText(data.getExtras().getString(
                        "PARAM_ACTIVITY2"));
                txtIpConectado.setTextColor(GREEN);*/
                break;
            }
            case 2: {
                /*txtIpConectado.setText(data.getExtras().getString(
                        "PARAM_ACTIVITY2"));
                txtIpConectado.setTextColor(RED);*/
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void changeTheme(@SuppressLint("SupportAnnotationUsage") @StyleRes final Resources.Theme theme){
        final Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_caixa, theme);
        imageView.setImageDrawable(drawable);
    }

    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados) {
            if(dados != null){
                if (dados.contains("--OK")){

                }
            }
            else{

            }
        }
    }

    //Recebe mensagens do servidor
    private void UpDateUI(Message msg){
        //Recebe mensagens para botões
        if(msg.what == 0){
            String txt = (String) msg.obj;
        }
    }
}
