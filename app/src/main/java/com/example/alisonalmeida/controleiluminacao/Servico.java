package com.example.alisonalmeida.controleiluminacao;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import static android.content.ContentValues.TAG;

public class Servico extends Service{
    int i, j;
    static boolean running;
    private boolean requisicao;
    Servidor servidor;
    static boolean run;
    private static boolean primeiraVez = true;
    private boolean service = false;
    //===============================================//
    ConnectivityManager gerConexao;
    NetworkInfo infoRede;
    SharedPreferences settings;
    //===============================================//
    public static final String PREFS_NAME = "ipServer";
    String IP;
    String KEY;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "iniciou");
        settings = getSharedPreferences(PREFS_NAME, 0);
        IP = settings.getString(KEY, "");
        Log.d(TAG, IP);

        running = true;
        i = 0;
        j = 0;
        run = true;
        servidor = new Servidor();

        service = true;

        this.gerConexao = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        this.infoRede = gerConexao.getActiveNetworkInfo();

        new WorkerThread().start();
        return super.onStartCommand(intent, flags, startId);
    }

    public Servico(){

    }

    public boolean isRun(){
        return run;
    }

    class WorkerThread extends Thread {
        public void run(){

            requisicao = false;
            Log.d("Inicio", "Iniciando");

            try{
                while (running){
                    Thread.sleep(5000);
                    //Tenta a reconexão com o servidor
                   if (!requisicao) {
                       if (!service) {
                           Log.d("MSG", "MSG: Buscando dados no IP: " + Servidor.getIP());
                           if (infoRede != null && infoRede.isConnected() && Servidor.getConectado()){
                               if (Servidor.isIpValido()){
                                   String url = "http://" + Servidor.getIP() + "/dados";
                                   requisicao = true;
                                   new solicitarDados().execute(url);
                               }
                               else{
                                   Log.d("MSG", "Não foi definido um IP valido");
                               }
                           }
                       }
                       else {
                           Log.d("MSG", "MSG: Buscando dados no IP: " + IP);
                           if (infoRede != null && infoRede.isConnected()){
                               if (!IP.isEmpty()){
                                   String url = "http://" + IP + "/dados";
                                   requisicao = true;
                                   new solicitarDados().execute(url);
                               }
                               else{
                                   Log.d("MSG", "Não foi definido um IP valido");
                               }
                           }
                       }
                   }
                }
            }
            catch (InterruptedException e){
                Log.e(TAG, e.getMessage(), e);
            }
        }
    }

    @Override
    public void onDestroy() {
        running = false;
        super.onDestroy();
    }

    private class solicitarDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            return Conexao.getDados(url[0]);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String dados) {
            Log.d(TAG, "Dados: " + dados);
            requisicao = false;

            if (dados != null && !dados.isEmpty()){
                //NotificationUtil.create(context, intent, "Nivel", "Nivel Baixo", 1);
                TratamentoDados(dados);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    void TratamentoDados (String data){
        String aux;
        int index = data.indexOf("/");
        String chuva = data.substring(index + 1);
        Log.d("Indexador", "Chuva: " + chuva);

        Context context = Servico.this;
        Intent intent = new Intent(context, Temperatura.class);

        if (chuva.equals("1") && primeiraVez){
            NotificationUtil.create(context, intent, "Chuva", "Alarme de Chuva", 1);
            primeiraVez = false;
        }

        else if (chuva.equals("0")){
            primeiraVez = true;
        }
    }
}
