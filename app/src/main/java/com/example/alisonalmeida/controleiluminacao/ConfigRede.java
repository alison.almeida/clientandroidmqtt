package com.example.alisonalmeida.controleiluminacao;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ConfigRede extends AppCompatActivity {
    public static final String PREFS_NAME = "ipServer";

    Button btnValidarIp;
    EditText txtIpServer;
    Vibrator v;
    ConnectivityManager gerConexao;
    NetworkInfo infoRede;
    String IP;

    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_rede);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        this.gerConexao = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        v = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        btnValidarIp = (Button) findViewById(R.id.btnValidarIP);
        txtIpServer = (EditText) findViewById(R.id.textIpServer);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String result = settings.getString("KEY", "");

        txtIpServer.setText(result);

        btnValidarIp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoRede = gerConexao.getActiveNetworkInfo();
                v.vibrate(100);

                IP = txtIpServer.getText().toString();

                if(IP.isEmpty()){
                    Log.d("DATA", "String Vazia");
                }


                if (infoRede != null && infoRede.isConnected()){
                    if (!IP.isEmpty()){
                        String url = "http://" + IP;
                        new solicitaDados().execute(url);
                    }
                    else{
                        alertShort("Insira um IP valido");
                    }
                }
                else{
                    alertShort("Nenhuma rede foi detectada");
                }
            }
        });
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        actionBar.getDisplayOptions();
        if (id == 16908332){
            finish();
            return true;
        }
        Log.d("Title", "Titulo: " + id);


        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    public void validar() {
        IP = txtIpServer.getText().toString();

        Intent it = new Intent();
        it.putExtra("IP_RESULT", IP);
        setResult(1, it);

        Servidor.setIP(IP);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("KEY", IP);
        editor.apply();

        super.onBackPressed();
    }
    private void alertShort(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    private void alertLong(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
        public void onBackPressed() {
        IP = txtIpServer.getText().toString();

        Intent it = new Intent();
        it.putExtra("IP_RESULT", IP);
        setResult(2, it);
        super.onBackPressed();
    }

    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados) {

            if(dados != null){
                if (dados.contains("--OK")){
                    alertShort("Conectado ao servidor");
                    validar();
                }
                else{
                    alertShort("Servdor não responde");
                }
            }
            else{
                alertShort("Servdor não responde");
            }
        }
    }
}