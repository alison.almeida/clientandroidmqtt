package com.example.alisonalmeida.controleiluminacao;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import static android.content.ContentValues.TAG;


public class main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String PREFS_NAME = "ipServer";
    private static final String PREFS = "Nomes";

    //==================== Variaveis Globais ===========================//
    String KEY;
    Button btnBloco1, btnBloco2, abrePortao, fechaPortao;
    TextView txtIpConectado;
    Vibrator v;
    ConnectivityManager gerConexao;
    NetworkInfo infoRede;
    Servidor servidor;
    String[] CHAVE = new String[8];
    String[] Results = new String[8];

    Servico servico = new Servico();

    //======= Objetos para salvar e carregar configurações ============//
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    //=================================================================//

    private Handler handler;            //Objeto que envia mensagens a Thread prnicipal

    //Cria um array com os 8 botões;
    Button[] btns = new Button[8];
    TextView[] txtEquipamentos = new TextView[8];
    int[] status = new int[8];

    int id, idTxtView;
    int i;
    int saidas;

    //==================================================================//
    int verde, vermelho;
    //==================================================================//

    boolean requisicao = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);                     //Seta o layout

        this.verde = getResources().getColor(R.color.botaoLigado);
        this.vermelho = getResources().getColor(R.color.botaoDesligado);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Iluminação");                                     //Altera o titulo da janela

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Inicia o serviço de vibração do celular.
        v = (Vibrator)getSystemService(VIBRATOR_SERVICE);

        //------------ cria o receptor de mensagens -------------
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                UpDateUI(msg);
            }
        };

        servidor = new Servidor(handler);
        //=======================================================

        this.btnBloco1 = (Button) findViewById(R.id.btnBloco1);
        this.btnBloco2 = (Button) findViewById(R.id.btnBloco2);
        this.abrePortao = (Button) findViewById(R.id.btnPortaoAbre);
        this.fechaPortao = (Button) findViewById(R.id.btnPortaoFecha);
        this.txtIpConectado = (TextView) findViewById(R.id.textIpConectado);

        id = R.id.btnLed1;

        for (i = 0; i < 8; i++){
            this.btns[i] = (Button) findViewById(id + i);
            status[i] = 0;
        }

        Log.d("Servico", ""+ servico.isRun());

        if (!servico.isRun()){
            //startService(new Intent(getBaseContext(), Servico.class));
            startService(new Intent(getContext(), Servico.class));
            Log.d("Inicio", "Tentando Iniciar Servico");
        }

        //============== Recupera os dados da aplicação ========================//
        settings = getSharedPreferences(PREFS_NAME, 0);
        String result = settings.getString(KEY, "");
        //======================================================================//

        //-------------- Cria as 8 KEYS de configurações -------- //
        for(i = 0; i < 8; i++){
            CHAVE[i] = "KEY " + i;
        }

        //---------- Instancia as TextsView's do layout --------------//
        idTxtView = R.id.txtDsp1;
        for (i = 0; i < 8; i++){
            this.txtEquipamentos[i] = (TextView) findViewById(idTxtView + i);
            this.txtEquipamentos[i].setGravity(0);
        }

        //------ Recupera os 8 dados salvos como parametros ------//
        for (i = 0; i < 8; i++){
            settings = getSharedPreferences(PREFS, 0);
            Results[i] = settings.getString(CHAVE[i], "");
            if (!Results[i].isEmpty()){
                this.txtEquipamentos[i].setText(Results[i]);
            }
        }

        txtIpConectado.setText(result);

        Log.d(TAG, "IP: " + result);

        if (result != null && result != ""){
            Servidor.setIP(result);
            Servidor.setIpValido(true);
        }
        else{
            Servidor.setIpValido(false);
        }

        this.gerConexao = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        this.infoRede = gerConexao.getActiveNetworkInfo();

        //Tenta a primeira conexão com o servidor
        if (infoRede != null && infoRede.isConnected()){
            if (Servidor.isIpValido()){
                String url = "http://" + Servidor.getIP();
                requisicao = true;
                new solicitaDados().execute(url);
            }
            else{
                alertShort("Não foi definido um IP válido");
            }
        }

        //----------------------- Acender Todos ----------------------//
        btnBloco1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v.vibrate(100);

                infoRede = gerConexao.getActiveNetworkInfo();

                if (infoRede != null && infoRede.isConnected() && Servidor.getConectado()){
                    if (Servidor.isIpValido()){
                        servidor.SETGPIO(255);
                        servidor.GETGPIO();
                    }
                    else{
                        alertShort("Não foi definido um IP valido");
                    }
                }
                else if (!Servidor.getConectado()){
                    alertShort("Não Conectado");
                }
                else{
                    alertlong("Nenhuma rede foi detectada");
                }
            }
        });

        //Abre portão
        abrePortao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v.vibrate(100);

                infoRede = gerConexao.getActiveNetworkInfo();

                if (infoRede != null && infoRede.isConnected() && Servidor.getConectado()){
                    if (Servidor.isIpValido()){
                        servidor.setPortao(1);
                    }
                    else{
                        alertShort("Não foi definido um IP valido");
                    }
                }
                else if (!Servidor.getConectado()){
                    alertShort("Não Conectado");
                }
                else{
                    alertlong("Nenhuma rede foi detectada");
                }
            }
        });

        //fecha portão
        fechaPortao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v.vibrate(100);

                infoRede = gerConexao.getActiveNetworkInfo();

                if (infoRede != null && infoRede.isConnected() && Servidor.getConectado()){
                    if (Servidor.isIpValido()){
                        servidor.setPortao(0);
                    }
                    else{
                        alertShort("Não foi definido um IP valido");
                    }
                }
                else if (!Servidor.getConectado()){
                    alertShort("Não Conectado");
                }
                else{
                    alertlong("Nenhuma rede foi detectada");
                }
            }
        });

        //------------------------- Apagar todos --------------------//
        btnBloco2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v.vibrate(100);

                infoRede = gerConexao.getActiveNetworkInfo();

                if (infoRede != null && infoRede.isConnected() && Servidor.getConectado()){
                    if (Servidor.isIpValido()){
                        servidor.SETGPIO(0);
                        servidor.GETGPIO();
                    }
                    else{
                        alertShort("Não foi definido um IP valido");
                    }
                }
                else if (!Servidor.getConectado()){
                    alertShort("Não Conectado");
                }
                else{
                    alertlong("Nenhuma rede foi detectada");
                }
            }
        });

        //------------- Cria o metodo para dispraro de click ----------//
        for (i = 0; i < 8; i++){
            this.btns[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int idx = v.getId();
                    btnClick(idx - id);
                }
            });
        }

    }

    @Override
    protected void onStop(){
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(KEY, txtIpConectado.getText().toString());
        editor.apply();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.renomearDispositivos) {
            Intent it = new Intent(main.this, RenomearDispositivos.class);
            it.putExtra("PARAM_ACTIVITY1", txtIpConectado
                    .getText().toString());
            startActivityForResult(it, 3);
            return true;
        }

        else if (id == R.id.configRede){

            Intent it = new Intent(main.this, ConfigRede.class);
            it.putExtra("PARAM_ACTIVITY1", txtIpConectado
                    .getText().toString());
            startActivityForResult(it, 1);
            return true;
        }

        else if (id == R.id.sobre){
            //alert("A&A Sistemas Eletronicos \n                   1.0");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_iluminacao) {

        }

        else if (id == R.id.nav_temp_humi) {
            Intent intent = new Intent(this, Temperatura.class);
            Bundle params = new Bundle();
            startActivity(intent);
            return true;
        }

        else if (id == R.id.nav_nivel) {
            Intent intent = new Intent(this, NivelAgua.class);
            Bundle params = new Bundle();
            startActivity(intent);
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Context getContext(){
        return this;
    }

    private void alertShort (String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }
    private void alertlong(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    //------------- Recebe os dados da Activity de Configurações de Rede ----------//
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case 1: {
                String dados = data.getExtras().getString("IP_RESULT");
                Log.d(TAG, "Resultado: " + dados);
                if (!dados.isEmpty()){
                    txtIpConectado.setText(dados);
                    txtIpConectado.setTextColor(verde);
                    Servidor.setIpValido(true);
                    Servidor.setConectado(true);

                    settings = getSharedPreferences(PREFS_NAME, 0);
                    editor = settings.edit();

                    editor.putString("KEY", dados);
                    editor.apply();

                    break;
                }
                else{
                    txtIpConectado.setText("");
                    Servidor.setIpValido(false);
                    Servidor.setConectado(false);
                    settings = getSharedPreferences(PREFS_NAME, 0);
                    editor = settings.edit();

                    editor.putString("KEY", dados);
                    editor.apply();
                    break;
                }
            }
            case 2: {
                String dados = data.getExtras().getString("IP_RESULT");

                if (!dados.isEmpty()){
                    txtIpConectado.setText(dados);
                    txtIpConectado.setTextColor(vermelho);
                    Servidor.setIpValido(true);
                    Servidor.setConectado(false);
                    new Reconexao().start();
                    settings = getSharedPreferences(PREFS_NAME, 0);
                    editor = settings.edit();

                    editor.putString("KEY", dados);
                    editor.apply();
                    break;
                }
                else{

                    txtIpConectado.setText("");
                    Servidor.setIpValido(false);
                    Servidor.setConectado(false);

                    settings = getSharedPreferences(PREFS_NAME, 0);
                    editor = settings.edit();

                    editor.putString("KEY", dados);
                    editor.apply();
                    break;
                }
            }
            case 3:{
                String[] dados = data.getExtras().getStringArray("NOMES");
                for (i = 0; i < 8; i++){
                    Log.d("Dados", "Dados["+i+"]: " + dados[i]);
                    if (!dados[i].isEmpty()){
                        settings = getSharedPreferences(PREFS, 0);
                        editor = settings.edit();
                        editor.putString(CHAVE[i], dados[i]);
                        editor.apply();

                        txtEquipamentos[i].setText(dados[i]);
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados) {

            if(dados != null){
                if (dados.contains("--OK")){
                    alertShort("Conectado ao servidor");
                    txtEditColor(verde);
                    servidor.GETGPIO();
                    Servidor.setConectado(true);

                }
                else{
                    alertShort("Servidor não responde");
                    txtEditColor(vermelho);
                    Servidor.setConectado(false);
                    new Reconexao().start();
                }
            }
            else{
                alertShort("Servidor não responde");
                txtEditColor(vermelho);
                Servidor.setConectado(false);
                new Reconexao().start();
            }
        }
    }

    public void btnClick(int id){

        for (i = 0; i<8; i++) {
            //Se o valor do indice for igual ao que for pressionado
            if (i == id){
                if (status[id] == 0) {
                    status[id] = 1;
                }

                else{
                    status[id] = 0;
                }
            }
        }

        saidas = status[0] + (status[1] * 2) + (status[2] * 4) + (status[3] * 8) + (status[4] * 16)
                + (status[5] * 32) + (status[6] * 64) + (status[7] * 128);

        v.vibrate(100);
        infoRede = gerConexao.getActiveNetworkInfo();

        if (infoRede != null && infoRede.isConnected() && Servidor.getConectado()){
            if (Servidor.isIpValido()){
                servidor.SETGPIO(saidas);
                servidor.GETGPIO();
            }
            else{
                alertShort("Não foi definido um IP valido");
            }
        }
        else if (!Servidor.getConectado()){
            alertShort("Não Conectado");
        }
        else{
            alertlong("Nenhuma rede foi detectada");
        }
    }

    private void UpDateUI(Message msg){
        //Recebe mensagens para botões ou alert
        String txt = (String) msg.obj;

        if(msg.what == 0){
            AtualizaBtns(Integer.parseInt(txt));
        }
        else if (msg.what == 3){
            if (txt.equals("Conectado")){
                txtEditColor(verde);
            }
            else{
                alertShort(txt);
                txtEditColor(vermelho);
                AtualizaBtns(0);
                Servidor.setConectado(false);
                new Reconexao().start();
            }
        }
    }

    public void AtualizaBtns(int val){
        int d = val;
        StringBuffer binario = new StringBuffer(); // guarda os dados
        i = 0;

        if (d > 0){
            while (d > 0) {
                int b = d % 2;
                binario.append(b);
                d = d >> 1;
                status[i] = b;
                i++;
            }
        }

        else{
            for (i = 0; i < 8; i++){
                status[i] = 0;
            }
        }

        for (i = 0; i < 8; i++){
            if (status[i] == 1){
                this.btns[i].setText("ON");
                this.btns[i].setBackgroundColor(getResources().getColor(R.color.botaoLigado));
            }
            else{
                this.btns[i].setText("OFF");
                this.btns[i].setBackgroundColor(getResources().getColor(R.color.botaoDesligado));
            }
        }
    }
    private void txtEditColor(int color){
        txtIpConectado.setTextColor(color);
    }

    class Reconexao extends Thread {
        public void run(){
            requisicao = false;
            try{
                while (!Servidor.getConectado()){
                    Thread.sleep(5000);
                    //Tenta a reconexão com o servidor
                    if (!requisicao) {
                        Log.d("MSG", "MSG: Tentando Conexao no IP: " + Servidor.getIP());
                        if (infoRede != null && infoRede.isConnected()){
                            if (Servidor.isIpValido()){
                                String url = "http://" + Servidor.getIP();
                                requisicao = true;
                                new Conectar().execute(url);
                            }
                            else{
                                alertShort("Não foi definido um IP válido");
                            }
                        }
                    }
                }
            }
            catch (InterruptedException e){
                Log.e(TAG, e.getMessage(), e);
            }
        }
    }

    private class Conectar extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados) {

            if(dados != null){
                if (dados.contains("--OK")){
                    alertShort("Conectado ao servidor");
                    txtEditColor(verde);
                    Servidor.setConectado(true);
                    servidor.GETGPIO();
                    requisicao = true;
                }
                else{
                    txtEditColor(vermelho);
                    Servidor.setConectado(false);
                    requisicao = false;
                }
            }
            else{
                requisicao = false;
                txtEditColor(vermelho);
                Servidor.setConectado(false);
            }
        }
    }
}
