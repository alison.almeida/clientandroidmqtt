package com.example.alisonalmeida.controleiluminacao;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.StyleRes;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import static android.content.ContentValues.TAG;


public class Temperatura extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    android.view.ContextThemeWrapper wrapper;

    ConnectivityManager gerConexao;
    NetworkInfo infoRede;
    private boolean EmReq = false, running;
    Display display = new Display();
    private int i, id, n, nP;
    ImageView[] imageViewTemperatura = new ImageView[6];
    ImageView[] imageViewHumidade = new ImageView[6];
    private Handler handler;
    Servidor servidor;
    int[] vF = new int[4];
    int[] vC = new int[4];
    int[] Humidade = new int[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_humidade);                     //Seta o layout

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Temperatura e Humidade");                                     //Altera o titulo da janela

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        n = R.drawable.ic_display_7_seg;
        nP = R.drawable.ic_display_7_seg_ponto;

        for(i = 0; i < 6; i++){
            this.imageViewTemperatura[i] = (ImageView) findViewById(display.getdTemperatura(i));
            if (i < 4) {
                if (i != 1){
                    wrapper = new android.view.ContextThemeWrapper(this, display.getNumber(0));
                    changeThemeT(wrapper.getTheme(), i, n);
                }
                else{
                    wrapper = new android.view.ContextThemeWrapper(this, display.getNumberP(0));
                    changeThemeT(wrapper.getTheme(), i, nP);
                }
            }
            else{
                if (i == 4){
                    wrapper = new android.view.ContextThemeWrapper(this, display.getSimbol());
                    changeThemeT(wrapper.getTheme(), i, n);
                }
                else{
                    wrapper = new android.view.ContextThemeWrapper(this, display.getLetter());
                    changeThemeT(wrapper.getTheme(), i, n);
                }
            }
        }

        for(i = 0; i < 6; i++){
            this.imageViewHumidade[i] = (ImageView) findViewById(display.getdHumidade(i));
            if (i < 4) {
                if (i != 1){
                    wrapper = new android.view.ContextThemeWrapper(this, display.getNumber(0));
                    changeThemeH(wrapper.getTheme(), i, n);
                }
                else{
                    wrapper = new android.view.ContextThemeWrapper(this, display.getNumberP(0));
                    changeThemeH(wrapper.getTheme(), i, nP);
                }
            }
        }

        //------------ cria o receptor de mensagens -------------
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                UpDateUI(msg);
            }
        };
        servidor = new Servidor(handler);

        this.gerConexao = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        this.infoRede = gerConexao.getActiveNetworkInfo();

        this.infoRede = gerConexao.getActiveNetworkInfo();

        if (infoRede != null && infoRede.isConnected()){
            if (Servidor.isIpValido()){
                servidor.getTemperatura();
                EmReq = true;
            }
            else{
                alertShort("Não foi definido um IP valido");
            }
        }
        else{
            alertlong("Nenhuma rede foi detectada");
        }

        running = true;
        new WorkerThread().start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.renomearDispositivos) {
            return true;
        }

        else if (id == R.id.configRede){
            return true;
        }

        else if (id == R.id.sobre){
            //alert("A&A Sistemas Eletronicos \n                   1.0");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_iluminacao) {
            Intent intent = new Intent(this, main.class);
            Bundle params = new Bundle();
            startActivity(intent);
            return true;
        }

        else if (id == R.id.nav_temp_humi) {

        }

        else if (id == R.id.nav_nivel) {
            Intent intent = new Intent(this, NivelAgua.class);
            Bundle params = new Bundle();
            startActivity(intent);
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Context getContext(){
        return this;
    }

    private void alertShort (String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }
    private void alertlong(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private void changeThemeT(@SuppressLint("SupportAnnotationUsage") @StyleRes final Resources.Theme theme, int nDisplay, int tDisplay){
        final Drawable drawable = ResourcesCompat.getDrawable(getResources(), tDisplay, theme);
        imageViewTemperatura[nDisplay].setImageDrawable(drawable);
    }

    private void changeThemeH(@SuppressLint("SupportAnnotationUsage") @StyleRes final Resources.Theme theme, int nDisplay, int tDisplay){
        final Drawable drawable = ResourcesCompat.getDrawable(getResources(), tDisplay, theme);
        imageViewHumidade[nDisplay].setImageDrawable(drawable);
    }

    private void UpDateUI(Message msg){
        //Recebe mensagens para botões
        EmReq = false;
        if(msg.what == 2){
            String txt = (String) msg.obj;
            if (!txt.isEmpty()){
                vF[0] = Integer.parseInt(txt.substring(0,1));
                vF[1] = Integer.parseInt(txt.substring(1,2));
                vF[2] = Integer.parseInt(txt.substring(3,4));
                vF[3] = Integer.parseInt(txt.substring(4,5));
                Humidade[0] = Integer.parseInt(txt.substring(6,7));
                Humidade[1] = Integer.parseInt(txt.substring(7,8));
                Humidade[2] = Integer.parseInt(txt.substring(9,10));
                Humidade[3] = Integer.parseInt(txt.substring(10));

                double tempF = (vF[0]*1000 + vF[1]*100 + vF[2]*10 + vF[3])/100;
                double tempC = (tempF - 32) * 0.56;

                AtualizaTemperatura();
                AtualizaHumidade();
            }
        }
    }
    public void AtualizaTemperatura(){
        for(i = 0; i < 4; i++){
            if (i != 1){
                wrapper = new android.view.ContextThemeWrapper(this, display.getNumber(vF[i]));
                changeThemeT(wrapper.getTheme(), i, n);
            }
            else{
                wrapper = new android.view.ContextThemeWrapper(this, display.getNumberP(vF[i]));
                changeThemeT(wrapper.getTheme(), i, nP);
            }
        }
    }

    public void AtualizaHumidade(){
        for(i = 0; i < 4; i++){
            if (i != 1){
                wrapper = new android.view.ContextThemeWrapper(this, display.getNumber(Humidade[i]));
                changeThemeH(wrapper.getTheme(), i, n);
            }
            else{
                wrapper = new android.view.ContextThemeWrapper(this, display.getNumberP(Humidade[i]));
                changeThemeH(wrapper.getTheme(), i, nP);
            }
        }
    }

    class WorkerThread extends Thread {
        public void run(){
            try{
                while (running){
                    Log.d("Msg:", "Rodando Thread");
                    Thread.sleep(3000);
                    if (!EmReq) {
                        infoRede = gerConexao.getActiveNetworkInfo();
                        if (infoRede != null && infoRede.isConnected()){
                            if (Servidor.isIpValido()){
                                servidor.getTemperatura();
                                EmReq = true;
                                Log.d("Msg:", "Enviando");
                            }
                        }
                    }
                }
            }
            catch (InterruptedException e){
                Log.e(TAG, e.getMessage(), e);
            }
        }
    }
    @Override
    public void onDestroy() {
        running = false;
        finish();
        super.onDestroy();
    }
    @Override
    public void onStop(){
        running = false;
        finish();
        super.onStop();
    }
}


//https://passafacil.sinetram.com.br/sbe-api-mobile/veiculos/133/1?apiKey=7ed79cf7-3a25-4d87-bf4a-bf0832ca9df