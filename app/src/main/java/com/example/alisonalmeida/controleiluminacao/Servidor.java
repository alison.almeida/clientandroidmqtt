package com.example.alisonalmeida.controleiluminacao;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class Servidor extends Thread{

    static private String IP;
    static private int info;
    static private String data;
    static private boolean ipValido;
    static private boolean conectado;

    /*
        - 0 informações sobre as saidas
        - 1 informações sobre o nivel de agua
        - 2 informações sobre a Temperatura e umidade;
        - 3 mensagem;
    */
    private Handler handler;

    public Servidor (Handler handler){
        this.handler = handler;
    }

    public Servidor(){

    }

    @Override
    public void run() {
        Message message = new Message();
        message.what = info;
        message.obj = data;
        handler.sendMessage(message);
    }

    public static boolean getConectado(){
        return conectado;
    }

    public static void setConectado(boolean b){
        conectado = b;
    }

    public static void setIP(String _IP){
           IP = _IP;
    }

    public static String getIP() {
        return IP;
    }

    public void SETGPIO(int value){

        String url = "http://" + IP + "/gpio?v=" + value;
        Log.d("URL", "URL: " + url);
        new ServidorSetGPIO().execute(url);
    }

    public void GETGPIO(){
        String url = "http://" + IP + "/gpio?status=0";
        new ServidorGetGPIO().execute(url);
    }

    public void getTemperatura(){
        String url = "http://" + IP + "/temperatura";
        new ServidorGetTemperatura().execute(url);
    }

    public void getNivel(){
        String url = "http://" + IP + "nivel";
        new ServidorGetNivel().execute(url);
    }

    public void getPortaoStatus(){

    }

    public void setPortao(int acao){
        String url = "http://" + IP + "/portao?set=" + acao;
        new ServidorSetPortao().execute(url);
    }

    static public boolean isIpValido() {
        return ipValido;
    }
    static public void setIpValido(boolean b){
        ipValido = b;
    }

    private class ServidorSetGPIO extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados) {
            if (dados != null) {
                if (dados.contains("--OK GPIO OK")){
                    info = 3;
                    data = "Conectado";
                    start();
                }
            }
            else{
                info = 3;
                data = "Servidor não responde";
                start();
            }
        }
    }

    private class ServidorGetGPIO extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados) {
            if (dados != null){
                info = 0;
                data = dados;
                start();
            }
        }
    }

    private class ServidorGetTemperatura extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados) {
            if (dados != null){
                info = 2;
                data = dados;
                start();
            }
        }
    }

    private class ServidorGetNivel extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... url){
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados){
            if (dados != null  && !dados.isEmpty()){
                info = 1;
                data = dados;
                start();
            }
        }
    }

    private class ServidorSetPortao extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... url){
            return Conexao.getDados(url[0]);
        }

        @Override
        protected void onPostExecute(String dados){
            if (dados != null  && !dados.isEmpty()){

            }
        }
    }
}
